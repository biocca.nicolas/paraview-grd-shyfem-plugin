# Paraview GRD Mesh Plugin

## Introduction

The Paraview GRD Mesh Plugin is a Python-based extension for Paraview, a popular data visualization and analysis tool. This plugin is designed to enable Paraview to read and write meshes in the GRD (SHYFEM) format, making easier to work with such data within Paraview.

## Features

- **Read GRD Meshes**: Import GRD mesh files into Paraview, making it accessible for visualization and analysis. It can read nonsorted meshes and bathymetry field as well as represent meshes on both cartesian and spherical coordinates. 

- **Write GRD Meshes**: Export Paraview meshes in the GRD format, preserving data integrity for use in other simulation tools.

- **Visualization Support**: Leverage Paraview's powerful visualization capabilities to explore and analyze GRD meshes.

## Installation

To install and use this plugin, follow these steps:

1. Clone or download this repo to your local machine.
2. Open Paraview.
3. Go to the "Tools" menu and select "Manage Plugins".
4. In the "Manage Plugins" dialog, click on the "Load New..." buttom. 
5. Navigate to the directory where you downloaded the plugin and select the Python script containing the script.
6. Click "OK" to load the plugin. 

**NOTE**: Additionally, you can check the option "Auto Load" to automatically load the plugin everytime you open Paraview. 

## Usage 

Once the plugin is installed you can read and write grd meshes as any other format natively supported by Paraview.

## License 

The Paraview GRD Mesh Plugin is licensed under the [MIT License](https://opensource.org/license/mit/). Feel free to modify and distribute according to the terms of this license.

#
# Paraview plugin
# SHYFEM grd mesh reader/writer
#
# Contact
#   Nicolás Biocca <nicolas.biocca@cmcc.it> 
#                  <biocca.nicolas@gmail.com>


import numpy as np
from paraview.util.vtkAlgorithm import (
    VTKPythonAlgorithmBase,
    smdomain,
    smhint,
    smproperty,
    smproxy,
)
from vtkmodules.numpy_interface import dataset_adapter as dsa
from vtkmodules.vtkCommonDataModel import vtkUnstructuredGrid

import paraview.vtk as vtk

import ctypes

#
# reader
#

@smproxy.reader(
    name="grd reader",
    extensions=['grd'],
    file_description="SHYFEM Files",
    support_reload=False,
)
class SHYFEMReader(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self, 
                                        nInputPorts=0, 
                                        nOutputPorts=1, 
                                        outputType="vtkUnstructuredGrid")
       
        self._filename     = None
        self._file_format  = None
        self._node_id      = list()
        self._element_id   = list()
        self._coords       = list()
        self._je           = list()
        self._bathymetry   = list()
        self._NB_NODES     = 0
        self._NB_ELEMS     = 0
        self._DATA_SORTED  = False
        self._useSphereProj= None
        
        

    @smproperty.stringvector(name="FileName")
    @smdomain.filelist()
    @smhint.filechooser(
        extensions=['grd'], file_description="SHYFEM Files"
    )
    def SetFileName(self, filename):
        if self._filename != filename:
            self._filename = filename
            self.Modified()

    @smproperty.stringvector(name="StringInfo", information_only="1")
    def GetStrings(self):
        return ['grd']

    @smproperty.stringvector(name="FileFormat", number_of_elements="1")
    @smdomain.xml(
        """
        <StringListDomain name="list">
            <RequiredProperties>
                <Property name="StringInfo" function="StringInfo"/>
            </RequiredProperties>
        </StringListDomain>
        """
    )
    def SetFileFormat(self, file_format):
        # Automatically deduce input format
        if file_format == "automatic":
            file_format = None

        if self._file_format != file_format:
            self._file_format = file_format
            self.Modified()

    @smproperty.xml("""
        <IntVectorProperty name="Use spherical coordinates"
                       command="SetSphericalCoordinates"
                       number_of_elements="1"
                       default_values="0">
        <BooleanDomain name="bool" />
        <Documentation>
            Use this checkbox to project mesh onto unit sphere.
        </Documentation>
        </IntVectorProperty>
    """)
    def SetSphericalCoordinates(self, value):
        self._useSphereProj = value
        self.Modified()



    def _RenumberConnectivity(self):
        """
        Procedure to renumber nodes ids based on connectivity je.
        """
        print('info: grd file not sorted')
        print('info: sorting connectivity')
        je = self._je
        node_id = self._node_id
        
        # Create a dictionary to map node IDs to their indices
        node_id_index = {node: idx for idx, node in enumerate(node_id)}

        # Use a list comprehension to renumber je
        #je = [node_id_index[je_i] + 1 for je_i in je] # Somehow it's not working
        for i in range(len(je)):
            je[i] = node_id_index[je[i]] + 1
        
    def RequestData(self, request, inInfoVec, outInfoVec):

        output = dsa.WrapDataObject(vtkUnstructuredGrid.GetData(outInfoVec))

        file = self._filename

        node_id      = self._node_id 
        element_id   = self._element_id
        coords       = self._coords
        je           = self._je
        bathymetry   = self._bathymetry
        NB_NODES     = self._NB_NODES
        NB_ELEMS     = self._NB_ELEMS
        DATA_SORTED  = self._DATA_SORTED

        # grd reader
        with open(file,'r') as f:
            for line in f:
                records = line.split()

                if not records:
                    continue
                    
                # support for fortran formatting style
                records = [record.upper().replace('D','E') for record in records]   # For fortran-like notation

                if records[0] == '1': # node tag
                    NB_NODES += 1
                    
                    node_id.append(int(records[1]))
                    
                    coords.append(float(records[3]))
                    coords.append(float(records[4]))

                    if len(records) == 6:
                        bathymetry.append(float(records[5]))

                if records[0] == '2':  # element tag
                    NB_ELEMS += 1

                    element_id.append(int(records[1]))

                    je.append(int(records[4]))
                    je.append(int(records[5]))
                    je.append(int(records[6]))

                    if len(records) == 8:                    # optional field -> bathymetry
                        bathymetry.append(float(records[7]))
                    
        # check if nodes/incidence are already sorted
        id_nodes_sorted =  np.linspace(1,NB_NODES,num=NB_NODES,dtype=int)
        if not np.all(id_nodes_sorted == node_id):
            self._RenumberConnectivity()
            DATA_SORTED = True
        
        # Points
        coords = np.reshape(coords,(-1,2))
        points = np.hstack([coords, np.zeros((NB_NODES,1))])
        
        if self._useSphereProj == 1:
            lon = coords[:,0]
            lat = coords[:,1]
            lat = np.deg2rad(lat)              
            lon = np.deg2rad(lon)            
                                             
            phi   = lon + np.pi              
            theta = lat - np.pi/2.0          
                                             
            x = np.sin(theta) * np.cos(phi)  
            y = np.sin(theta) * np.sin(phi)  
            z = np.cos(theta)                
            points = np.column_stack([x,y,z])

        output.SetPoints(points)

        # Cells
        # Cast flatten je into 2-dimensional array
        je = np.array(je, dtype=np.int32)
        je = np.reshape(je,(-1,3))
        je = je - 1  # zero-based numbering

        cell_types = np.full(NB_ELEMS, vtk.VTK_TRIANGLE, dtype=np.ubyte)
        cell_offsets = (1 + 3) * np.arange(NB_ELEMS, dtype=np.int32)
        cell_conn = np.hstack(
            [3 * np.ones((NB_ELEMS, 1), dtype=np.int32), je]
        ).flatten()

        output.SetCells(cell_types, cell_offsets, cell_conn)

        # Bathymetrhy on cells
        if len(bathymetry) == NB_ELEMS:
            bathymetry = np.array(bathymetry)
            output.CellData.append(bathymetry, 'bathymetry')
        
        # for point data we could use
        if len(bathymetry) == NB_NODES:
            bathymetry = np.array(bathymetry)
            output.PointData.append(bathymetry, 'bathymetry')

        if DATA_SORTED:    # Save original node/cell ids
            output.CellData.append(np.array(element_id), 'cell_id')
            output.PointData.append(np.array(node_id), 'node_id')

        return 1

#
# Writer
# 

@smproxy.writer(
    name="shyfem writer",
    extensions=['grd'],
    file_description="shyfem grd file",
    support_reload=False,
)
@smproperty.input(name="Input", port_index=0)
@smdomain.datatype(dataTypes=["vtkUnstructuredGrid"], composite_data_supported=False)
class SHYFEMWriter(VTKPythonAlgorithmBase):
    def __init__(self):
        VTKPythonAlgorithmBase.__init__(self, 
                                        nInputPorts=1, 
                                        nOutputPorts=0, 
                                        inputType="vtkUnstructuredGrid")
        self._filename = None
        self._scalarArray = None


    @smproperty.stringvector(name="FileName", panel_visibility="never")
    @smdomain.filelist()
    def SetFileName(self, filename):
        if self._filename != filename:
            self._filename = filename
            self.Modified()
    
    @smproperty.stringvector(name="Arrays", number_of_elements_per_command=1, \
                             repeat_command=1, clean_command="ClearChosenArrays")
    @smdomain.xml("""
        <ArrayListDomain attribute_type="Scalars" name="array_list">
          <RequiredProperties>
            <Property function="Input" name="Input" />
          </RequiredProperties>
        </ArrayListDomain>
    """)
    def ChooseArray(self, name):
        self._scalarArray = name


    def ClearChosenArrays(self):
        self._scalarArray = None

    def RequestData(self, request, inInfoVec, outInfoVec):
        
        mesh = dsa.WrapDataObject(vtkUnstructuredGrid.GetData(inInfoVec[0]))

        # Read points
        points = np.asarray(mesh.GetPoints())
        #points = np.asarray(mesh.GetPoints().GetData())   # if no using dsa module (dataset_adapter)


        cell_conn = np.asarray(mesh.GetCells())

        cell_conn = np.reshape(cell_conn,(-1,4))  # reshape data. Each row has is formatted as 3 node1 node2 node3
        cell_conn[:,1:] = cell_conn[:,1:] + 1     # 0-based to 1-based numbering

        NB_NODES = points.shape[0]
        NB_ELEMS = cell_conn.shape[0]
        id_nodes = np.linspace(1,NB_NODES,num=NB_NODES,dtype=int)
        id_elems = np.linspace(1,NB_ELEMS,num=NB_ELEMS,dtype=int)

        # bathymetry field
        if self._scalarArray is not None:
            EXIST_BATHYMETRY = True
            field_name = self._scalarArray
            try:
                bathymetry = np.asarray(mesh.GetCellData().GetArray(field_name))
            except:
                pass
            try:
                bathymetry = np.asarray(mesh.GetPointData().GetArray(field_name))
            except:
                pass
        else:
            EXIST_BATHYMETRY = False

        # is bathy at nodes or elems?
        BATHY_ELEMS = False
        BATHY_NODES = False
        if EXIST_BATHYMETRY:
            if len(bathymetry) == NB_NODES:
                BATHY_NODES = True
            if len(bathymetry) == NB_ELEMS:
                BATHY_ELEMS = True


        # exporting grd
        zeros = np.zeros(NB_NODES,dtype=int)
        x = points[:,0]
        y = points[:,1]
        block_nodes = np.array( (zeros+1, id_nodes, zeros, x, y) ).T
        if BATHY_NODES:
            block_nodes = np.array( (zeros+1, id_nodes, zeros, x, y, bathymetry) ).T

        zeros = np.zeros(NB_ELEMS,dtype=int)
        if BATHY_ELEMS:
            block_elems = np.array( (zeros+2, id_elems, zeros, cell_conn[:,0], cell_conn[:,1], cell_conn[:,2], cell_conn[:,3], bathymetry) ).T
        else:
            block_elems = np.array( (zeros+2, id_elems, zeros, cell_conn[:,0], cell_conn[:,1], cell_conn[:,2], cell_conn[:,3]) ).T

        
        filename = self._filename
        print(f'filename = {filename}')
        with open(filename,'w') as f:
            output_format = '%1d %11d %1d %12.8e %12.8e'
            if BATHY_NODES:
                output_format = output_format + ' %12.8e'
            np.savetxt(f, block_nodes, fmt=output_format)
            f.write('\n')
            output_format = '%1d %11d %1d %1d %11d %11d %11d'
            if BATHY_ELEMS:
                output_format = output_format + ' %12.8e'
            np.savetxt(f, block_elems, fmt=output_format)
            f.write('\n')
        
        return 1
    
    def Write(self):
        self.Modified()
        self.Update()
